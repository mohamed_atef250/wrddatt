package com.yumaas.flowersproject.user;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.like.LikeButton;
import com.like.OnLikeListener;
import com.squareup.picasso.Picasso;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.admin.AdminFlowersFragment;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.databinding.FragmentFlowerDetailsBinding;
import com.yumaas.flowersproject.models.Flower;
import com.yumaas.flowersproject.models.Order;
import com.yumaas.flowersproject.models.User;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class FlowerDetailsActivity extends AppCompatActivity {

    FragmentFlowerDetailsBinding binding;
    Flower flower;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.fragment_flower_details);

         flower = (Flower)getIntent().getSerializableExtra("flower");
        binding.name.setText(flower.name);
        binding.details.setText(flower.details);
        binding.price.setText(" السعر "+flower.price+" ريال سعودي");
        Picasso.with(this).load(flower.image).into(binding.imageView);

        binding.fav.setLiked(DataBaseHelper.getSavedUser().checkFavourites(flower.id));

        binding.fav.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {

                User user = DataBaseHelper.getSavedUser();
                user.addFavourites(flower.id);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                User user = DataBaseHelper.getSavedUser();
                user.deleteFavourites(flower.id);
            }
        });

        binding.buyNow.setOnClickListener(view -> buyDialog());


    }


    private void buyDialog(){
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(FlowerDetailsActivity.this);
        builderSingle.setIcon(R.drawable.ic_cart);
        builderSingle.setTitle("من فضلك اختر الكميه");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(FlowerDetailsActivity.this, android.R.layout.select_dialog_singlechoice);

        for(int i=0; i<10; i++) {
            arrayAdapter.add(""+i);
        }

        builderSingle.setNegativeButton("خروج", (dialog, which) -> dialog.dismiss());

        builderSingle.setAdapter(arrayAdapter, (dialog, which) -> {
            String strName = arrayAdapter.getItem(which);
            AlertDialog.Builder builderInner = new AlertDialog.Builder(FlowerDetailsActivity.this);
            builderInner.setMessage("هل تريد شراء "+strName+"  من "+flower.name);
            builderInner.setTitle(" تاكيد شراء الورد ");
            builderInner.setPositiveButton("تاكيد", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog,int which) {
                    DataBaseHelper.addOrder(new Order(DataBaseHelper.getSavedUser(),flower,Integer.parseInt(strName)));
                    dialog.dismiss();

                    SweetDialogs.successMessage(FlowerDetailsActivity.this, "تم اضافه الورد بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                         }
                    });


                }
            });
            builderInner.show();
        });
        builderSingle.show();
    }

}
