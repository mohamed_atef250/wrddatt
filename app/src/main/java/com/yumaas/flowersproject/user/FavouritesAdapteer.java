package com.yumaas.flowersproject.user;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.flowersproject.R;

import java.util.ArrayList;


public class FavouritesAdapteer extends RecyclerView.Adapter<FavouritesAdapteer.ViewHolder> {

    //All methods in this adapter are required for a bare minimum recyclerview adapter
    OnItemClickListener onItemClickListener;
    ArrayList<TwoImages> images;

    int preivious = 0;
    int current = 0;

    // Constructor of the class
    public FavouritesAdapteer(OnItemClickListener onItemClickListener, ArrayList<TwoImages> images) {
        this.onItemClickListener = onItemClickListener;


        this.images = images;
    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return images.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flower, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.imageView.setImageResource(images.get(position).img1);
        holder.imageView2.setImageResource(images.get(position).img2);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClickListener(position);
            }
        });

    }

    // Static inner class to initialize the views of rows
    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, imageView2;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            imageView2 = view.findViewById(R.id.imageView2);

        }
    }
}