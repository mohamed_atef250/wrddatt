package com.yumaas.flowersproject.user;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.models.Flower;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class FlowersAdapter extends RecyclerView.Adapter<FlowersAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Flower> flowers;

    public FlowersAdapter(OnItemClickListener onItemClickListener, ArrayList<Flower> flowers) {
        this.onItemClickListener = onItemClickListener;
        this.flowers = flowers;
    }


    @Override
    public int getItemCount() {
        return flowers==null?0:flowers.size();
    }



    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flower, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Picasso.with(holder.imageView.getContext()).load(flowers.get(position).image).into(holder.imageView);
        holder.text.setText(flowers.get(position).name);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClickListener(position);
            }
        });
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView text;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            text = view.findViewById(R.id.text);


        }
    }
}