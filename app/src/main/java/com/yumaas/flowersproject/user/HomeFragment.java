package com.yumaas.flowersproject.user;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.models.Ads;
import com.yumaas.flowersproject.models.Flower;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment {
    private View rootView;
    private SliderLayout mDemoSlider;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        mDemoSlider = (SliderLayout)rootView.findViewById(R.id.slider);

        HashMap<String,String> url_maps;
        url_maps = new HashMap<>();

          ArrayList<Ads> ads=DataBaseHelper.getDataLists().ads;



        for(int i=0; i< ads.size(); i++){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView
                    .description(ads.get(i).text)
                    .image(ads.get(i).image)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",ads.get(i).text);
            textSliderView.getBundle()
                    .putString("link",ads.get(i).link);
            textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {

                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(slider.getBundle().getString("link")));
                        startActivity(browserIntent);
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }
            });
            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);



        List<String> names = new ArrayList<>();
        names.add("اكاليل");
        names.add("باقات أفراح");
        names.add("باقات أرجوانية");
        names.add("ورد صناعي");
        names.add("أزهار عيد الميلاد");
        names.add("ورد طبيعي");


        FragmentPagerItems.Creator creator =  FragmentPagerItems.with(getActivity());

        for(int i=0; i<names.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putInt("cat_id",i);
            creator.add(names.get(i), FlowersFragment.class,bundle);
        }

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(getChildFragmentManager(),creator
                .create());

        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) rootView.findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);


        return rootView;
    }

}