package com.yumaas.flowersproject.user;

public class TwoImages {
    public  int img1;
    public  int img2;
    String text;

    public TwoImages(int img1, int img2){
        this.img1  = img1;
        this.img2 = img2;
    }

    public TwoImages(int img1, String text){
        this.img1  = img1;
        this.text = text;
    }
}
