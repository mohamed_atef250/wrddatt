package com.yumaas.flowersproject.user;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
