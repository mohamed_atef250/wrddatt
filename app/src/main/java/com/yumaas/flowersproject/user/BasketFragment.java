package com.yumaas.flowersproject.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.models.Order;
import com.yumaas.flowersproject.models.User;

import java.util.ArrayList;


public class BasketFragment extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_cart, container, false);

        RecyclerView recyclerView =  rootView.findViewById(R.id.recycler_view);
        final ArrayList<Order> images = new ArrayList<>();
        final ArrayList<Order> orders = DataBaseHelper.getDataLists().orders;
        User user = DataBaseHelper.getSavedUser();

        for(int i=0; i<orders.size(); i++) {
            if(orders.get(i).user.id==user.id){
                images.add(orders.get(i));
            }
        }

        //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        BasketAdapter categoriesAdapter1 = new BasketAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                startActivity(new Intent(getActivity(),FlowerDetailsActivity.class));
            }
        }, images);
        recyclerView.setAdapter(categoriesAdapter1);


        return rootView;
    }

}