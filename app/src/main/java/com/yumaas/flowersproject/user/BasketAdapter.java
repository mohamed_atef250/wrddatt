package com.yumaas.flowersproject.user;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.models.Order;

import java.util.ArrayList;


public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.ViewHolder> {

    //All methods in this adapter are required for a bare minimum recyclerview adapter
    OnItemClickListener onItemClickListener;
    ArrayList<Order> images;



    // Constructor of the class
    public BasketAdapter(OnItemClickListener onItemClickListener, ArrayList<Order> images) {
        this.onItemClickListener = onItemClickListener;


        this.images = images;
    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return images.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_basket, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Picasso.with(holder.itemView.getContext()).load(images.get(position).flower.image).into(holder.imageView);
        holder.name.setText(images.get(position).flower.name);
        holder.details.setText(images.get(position).flower.details);
        holder.price.setText(" سعر البوكيه : " + images.get(position).flower.price + " ريال سعودي ");
        holder.quantity.setText(" الكميه : " + images.get(position).quantity + " ورده ");

        try {
            int total = Integer.parseInt(images.get(position).flower.price) * images.get(position).quantity;
            holder.total.setText(" الاجمالي : " + total + " ريال سعودي ");
        } catch (Exception e){
            e.getStackTrace();
        }


    }

    // Static inner class to initialize the views of rows
    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name,details,price,quantity,total;



        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            name  = view.findViewById(R.id.name);
            details = view.findViewById(R.id.details);
            price = view.findViewById(R.id.price);
            quantity = view.findViewById(R.id.quantity);
            total = view.findViewById(R.id.total);
        }
    }
}