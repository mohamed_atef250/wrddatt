package com.yumaas.flowersproject.user;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.models.Flower;
import com.yumaas.flowersproject.models.User;

import java.util.ArrayList;

public class FavouritesFragment extends Fragment {
    private View rootView;

      ArrayList<Flower>images;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_flowers, container, false);

        RecyclerView recyclerView =  rootView.findViewById(R.id.recycler_view);
         images = new ArrayList<>();
        ArrayList<Flower> flowers =  DataBaseHelper.getDataLists().flowers;

        User user = DataBaseHelper.getSavedUser();


        for(int i=0; i<flowers.size(); i++) {
            if(user.checkFavourites(flowers.get(i).id)){
                images.add(flowers.get(i));
            }
        }


        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        FlowersAdapter categoriesAdapter1 = new FlowersAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                Intent intent = new Intent(getActivity(),FlowerDetailsActivity.class);
                intent.putExtra("flower",images.get(position));
                startActivity(intent);
            }
        }, images);
        recyclerView.setAdapter(categoriesAdapter1);

        return rootView;
    }

}