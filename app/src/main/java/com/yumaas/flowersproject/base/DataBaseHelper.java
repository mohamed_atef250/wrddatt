package com.yumaas.flowersproject.base;


import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.yumaas.flowersproject.models.Ads;
import com.yumaas.flowersproject.models.Flower;
import com.yumaas.flowersproject.models.Order;
import com.yumaas.flowersproject.models.User;
import com.yumaas.flowersproject.volleyutils.MyApplication;

import java.util.ArrayList;



public class DataBaseHelper {

    private static SharedPreferences sharedPreferences = null;


    private DataBaseHelper() {

    }

    public static SharedPreferences getSharedPreferenceInstance() {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences = MyApplication.getInstance().getApplicationContext().getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }


    public static void addUser(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users == null) {
            users = new ArrayList<>();
        }
        users.add(student);
        dataLists.users = users;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }



    public static User loginUser(String email,String password) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).email.equals(email)&&dataLists.users.get(i).password.equals(password)) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }

    public static User findUser(int id) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id == id ) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }


    public static void updateUser(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {

            dataLists.users = users;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id==student.id) {
                    dataLists.users.set(i,student);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void removeUser(User helper) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            dataLists.users = users;
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id==helper.id) {
                    dataLists.users.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }


    public static int generateId() {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        int id = getSharedPreferenceInstance().getInt("id",1);
        id++;
        prefsEditor.putInt("id", id);
        prefsEditor.apply();
        
        return id;
    }
    


    public static DataLists getDataLists() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("users", "");
        if (json.equals("")) return new DataLists();
        return gson.fromJson(json, DataLists.class);
    }


    public static User getSavedUser() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("savedUser", "");
        return gson.fromJson(json, User.class);
    }

    public static void saveStudent(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(student);
        prefsEditor.putString("savedUser", json);
        prefsEditor.apply();
    }







    public static void addAds(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ads == null) {
            ads = new ArrayList<>();
        }
        ads.add(ad);
        dataLists.ads = ads;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }


    public static void updateAds(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ads != null) {

            dataLists.ads = ads;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.ads.get(i).id==ad.id) {
                    dataLists.ads.set(i,ad);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void removeAd(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ad != null) {
            dataLists.ads = ads;
            for (int i = 0; i < dataLists.ads.size(); i++) {
                if (dataLists.ads.get(i).id==ad.id) {
                    dataLists.ads.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void addFlowers(Flower flower) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Flower> flowers = dataLists.flowers;
        if (flowers == null) {
            flowers = new ArrayList<>();
        }
        flowers.add(flower);
        dataLists.flowers = flowers;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }


    public static void updateAds(Flower flower) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Flower> flowers = dataLists.flowers;
        if (flowers != null) {

            dataLists.flowers = flowers;

            for (int i = 0; i < dataLists.flowers.size(); i++) {
                if (dataLists.flowers.get(i).id==flower.id) {
                    dataLists.flowers.set(i,flower);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void removeAd(Flower flower) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Flower> flowers = dataLists.flowers;
        if (flower != null) {
            dataLists.flowers = flowers;
            for (int i = 0; i < dataLists.flowers.size(); i++) {
                if (dataLists.flowers.get(i).id==flower.id) {
                    dataLists.flowers.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }


    public static void addOrder(Order order) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Order> orders = dataLists.orders;
        if (orders == null) {
            orders = new ArrayList<>();
        }
        orders.add(order);
        dataLists.orders = orders;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }




}
