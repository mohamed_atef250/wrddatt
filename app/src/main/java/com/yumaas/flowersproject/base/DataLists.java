package com.yumaas.flowersproject.base;

import com.yumaas.flowersproject.models.Ads;
import com.yumaas.flowersproject.models.Flower;
import com.yumaas.flowersproject.models.Order;
import com.yumaas.flowersproject.models.User;

import java.util.ArrayList;


public class DataLists {

    public ArrayList<User> users = new ArrayList<>();
    public ArrayList<Ads> ads = new ArrayList<>();
    public ArrayList<Flower> flowers = new ArrayList<>();
    public ArrayList<Order> orders = new ArrayList<>();

}
