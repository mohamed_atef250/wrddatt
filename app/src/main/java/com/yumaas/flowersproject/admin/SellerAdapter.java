package com.yumaas.flowersproject.admin;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.ShowLocationActivity;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.models.User;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class SellerAdapter extends RecyclerView.Adapter<SellerAdapter.ViewHolder> {

    Context context;
    ArrayList<User> sellers;
    boolean showDelete;

    public SellerAdapter(Context context, ArrayList<User> sellers,boolean showDelete) {
        this.context = context;
        this.sellers = sellers;
        this.showDelete=showDelete;
    }


    @Override
    public SellerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        SellerAdapter.ViewHolder viewHolder = new SellerAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SellerAdapter.ViewHolder holder, final int position) {

        holder.name.setText(sellers.get(position).name);
        holder.phone.setText(sellers.get(position).phone);
        holder.email.setText(sellers.get(position).email);

        if(showDelete){
            holder.showLocation.setVisibility(View.VISIBLE);
        }else{
            holder.showLocation.setVisibility(View.GONE);

        }

        holder.showLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ShowLocationActivity.class);
                intent.putExtra("lat",sellers.get(position).lat);
                intent.putExtra("lng",sellers.get(position).lng);
                view.getContext().startActivity(intent);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد المسح")
                        .setContentText("بالتاكيد مسح هذا العنصر ؟")
                        .setConfirmText("نعم امسح").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        DataBaseHelper.removeUser(sellers.get(position));
                        sellers.remove(position);
                        notifyDataSetChanged();

                        try {
                            sweetAlertDialog.cancel();
                            sweetAlertDialog.dismiss();
                        } catch (Exception e) {
                            e.getStackTrace();
                        }

                    }
                })
                        .show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return sellers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, phone, email;
        ImageView delete;
        Button showLocation;


        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            phone = itemView.findViewById(R.id.phone);
            email = itemView.findViewById(R.id.email);
            delete = itemView.findViewById(R.id.delete);
            showLocation = itemView.findViewById(R.id.showLocation);
        }
    }
}
