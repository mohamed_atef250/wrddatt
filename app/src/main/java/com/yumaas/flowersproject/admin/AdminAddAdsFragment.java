package com.yumaas.flowersproject.admin;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.flowersproject.ImageResponse;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.base.Validate;
import com.yumaas.flowersproject.base.filesutils.FileOperations;
import com.yumaas.flowersproject.base.filesutils.VolleyFileObject;
import com.yumaas.flowersproject.databinding.FragmentAddAdsBinding;
import com.yumaas.flowersproject.databinding.FragmentAddFlowerBinding;
import com.yumaas.flowersproject.models.Ads;
import com.yumaas.flowersproject.models.Flower;
import com.yumaas.flowersproject.user.FlowerDetailsActivity;

import com.yumaas.flowersproject.user.FragmentHelper;
import com.yumaas.flowersproject.user.OnItemClickListener;
import com.yumaas.flowersproject.user.SweetDialogs;
import com.yumaas.flowersproject.user.TwoImages;
import com.yumaas.flowersproject.volleyutils.ConnectionHelper;
import com.yumaas.flowersproject.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AdminAddAdsFragment extends Fragment {
    private View rootView;
    FragmentAddAdsBinding fragmentAddFlowerBinding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentAddFlowerBinding   = DataBindingUtil
                .inflate(inflater, R.layout.fragment_add_ads, container, false);

        rootView = fragmentAddFlowerBinding.getRoot();


        fragmentAddFlowerBinding. image.setFocusable(false);
        fragmentAddFlowerBinding.  image.setClickable(true);

        fragmentAddFlowerBinding.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
            }
        });

        fragmentAddFlowerBinding.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    DataBaseHelper.addAds(new Ads(selectedImage
                            ,fragmentAddFlowerBinding.link.getText().toString()
                            ,fragmentAddFlowerBinding.name.getText().toString()));

                    SweetDialogs.successMessage(getActivity(), "تم اضافه الاعلان بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            FragmentHelper.popAllFragments(requireActivity());
                            FragmentHelper.addFragment(requireActivity(),new AdminAdsFragment(),"AdminAdsFragment");
                        }
                    });

                }
            }
        });

        return rootView;
    }


    private boolean validate(){

        if(Validate.isEmpty(fragmentAddFlowerBinding.name.getText().toString())
                ||Validate.isEmpty(fragmentAddFlowerBinding.link.getText().toString())
                ||Validate.isEmpty(fragmentAddFlowerBinding.image.getText().toString())){
            SweetDialogs.errorMessage(getContext(),"من فضلك قم بملا جميع البيانات");
            return false;
        }

        return true;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                FileOperations.getVolleyFileObject(getActivity(), data, "image",
                        43);


        fragmentAddFlowerBinding.image.setText("تم اضافه الهويه بنجاح");

        volleyFileObjects.add(volleyFileObject);



        addServiceApi();


    }


    String selectedImage="";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta",""+selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }




}