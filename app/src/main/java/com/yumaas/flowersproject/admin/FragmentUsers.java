package com.yumaas.flowersproject.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.models.User;
import com.yumaas.flowersproject.user.FragmentHelper;

import java.util.ArrayList;

import static android.view.View.OnClickListener;


public class FragmentUsers extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_children, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);
        ArrayList<User>users = DataBaseHelper.getDataLists().users;
        ArrayList<User>usersTemp = new ArrayList<>();

        for(int i=0; i<users.size(); i++){
            if(users.get(i).type != 2){
                usersTemp.add(users.get(i));
            }
        }

        SellerAdapter menuAdapter = new SellerAdapter(getActivity(),usersTemp,false);
        menuList.setAdapter(menuAdapter);
        rootView.findViewById(R.id.add).setVisibility(View.GONE);
        rootView.findViewById(R.id.add).setOnClickListener(view -> FragmentHelper.addFragment(getActivity(),new FragmentAddSeller(),"FragmentAddChild"));


        return rootView;
    }


}
