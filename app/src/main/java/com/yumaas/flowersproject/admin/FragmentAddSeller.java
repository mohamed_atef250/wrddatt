package com.yumaas.flowersproject.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.SelectLocationActivity;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.base.filesutils.VolleyFileObject;
import com.yumaas.flowersproject.models.User;
import com.yumaas.flowersproject.user.FragmentHelper;

import java.util.ArrayList;



public class FragmentAddSeller extends Fragment {
    private View rootView;
    private EditText name, userName, email, phone, password,idNumber,location;
    private Button add;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_children, container, false);
        name = rootView.findViewById(R.id.name);
        userName = rootView.findViewById(R.id.user_name);
        email = rootView.findViewById(R.id.email);
        phone = rootView.findViewById(R.id.phone);
        password = rootView.findViewById(R.id.password);
        idNumber = rootView.findViewById(R.id.national_id);
        location = rootView.findViewById(R.id.location);

        location.setFocusable(false);
        location.setClickable(true);

        location.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), SelectLocationActivity.class);
            startActivityForResult(intent, 321);
        });



        add = rootView.findViewById(R.id.btn);



        add.setOnClickListener(view -> {
            User user = new User(name.getText().toString(), email.getText().toString(), phone.getText().toString(), password.getText().toString());
            user.type=2;
            user.lat=lat;
            user.lng=lng;
            user.userName=userName.getText().toString();
            user.idNumber=idNumber.getText().toString();
            DataBaseHelper.addUser(user);
            Toast.makeText(getActivity(), "تمت الاضافه بنجاح", Toast.LENGTH_SHORT).show();
            FragmentHelper.replaceFragment(getActivity(), new FragmentSeller(), "FragmentChildren");

        });

        return rootView;
    }


    public double lat, lng;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        location.setText("تم اختيار الموقع بنجاح");
        lat = data.getDoubleExtra("lat", 0.0);
        lng = data.getDoubleExtra("lng", 0.0);

    }





}
