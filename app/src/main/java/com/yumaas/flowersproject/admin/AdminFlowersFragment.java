package com.yumaas.flowersproject.admin;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.databinding.FragmentListBinding;
import com.yumaas.flowersproject.user.FlowerDetailsActivity;
import com.yumaas.flowersproject.user.FragmentHelper;
import com.yumaas.flowersproject.user.OnItemClickListener;


public class AdminFlowersFragment extends Fragment {
    private View rootView;
    FragmentListBinding fragmentListBinding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentListBinding  = DataBindingUtil
                .inflate(inflater, R.layout.fragment_list, container, false);

        fragmentListBinding.btnAdd.setVisibility(View.VISIBLE);
        fragmentListBinding.btnAdd.setText("اضافه ورده");

        fragmentListBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(requireActivity(),new AddFlowerFragment(),"AdminFlowersFragment");
            }
        });

        RecyclerView recyclerView =  fragmentListBinding.recyclerView;

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));

        AdminFlowersAdapter categoriesAdapter1 = new AdminFlowersAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, DataBaseHelper.getDataLists().flowers);
        recyclerView.setAdapter(categoriesAdapter1);
        rootView=fragmentListBinding.getRoot();
        return rootView;
    }

}