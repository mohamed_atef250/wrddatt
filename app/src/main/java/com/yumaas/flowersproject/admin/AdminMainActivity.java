package com.yumaas.flowersproject.admin;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;

import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.user.FragmentHelper;
import com.yumaas.flowersproject.user.HomeFragment;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import nl.joery.animatedbottombar.AnimatedBottomBar;

public class

AdminMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        AnimatedBottomBar animatedBottomBar = findViewById(R.id.bottom_bar);
        animatedBottomBar.setOnTabSelectListener(new AnimatedBottomBar.OnTabSelectListener() {
            @Override
            public void onTabSelected(int i, @Nullable AnimatedBottomBar.Tab tab, int i1, @NotNull AnimatedBottomBar.Tab tab1) {
                FragmentHelper.popAllFragments(AdminMainActivity.this);

                if (i1 == 0) {
                    FragmentHelper.addFragment(AdminMainActivity.this, new AdminFlowersFragment(), "FlowersFragment");
                } else if (i1 == 1) {
                    FragmentHelper.addFragment(AdminMainActivity.this, new AdminAdsFragment(), "AdminAdsFragment");
                } else if (i1 == 2) {
                    FragmentHelper.addFragment(AdminMainActivity.this, new AdminBasketFragment(), "AdminOrdersFragment");
                } else if (i1 == 3) {
                    FragmentHelper.addFragment(AdminMainActivity.this, new FragmentSeller(), "FragmentAddSeller");
                }else if (i1 == 4) {
                    FragmentHelper.addFragment(AdminMainActivity.this, new FragmentUsers(), "FragmentUsers");
                }
                else {
                    finish();
                }
            }

            @Override
            public void onTabReselected(int i, @NotNull AnimatedBottomBar.Tab tab) {

            }
        });
        FragmentHelper.addFragment(this, new AdminFlowersFragment(), "HomeFragment");
    }

    boolean doubleBackToExitPressedOnce = true;

    @Override
    public void onBackPressed() {

        if (!doubleBackToExitPressedOnce) {
            finish();
        } else {

            this.doubleBackToExitPressedOnce = false;
            Toast.makeText(this, "اشغط مرتين للخروج", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = true, 2000);
        }

    }

}