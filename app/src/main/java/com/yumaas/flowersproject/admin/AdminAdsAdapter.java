package com.yumaas.flowersproject.admin;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.models.Ads;
import com.yumaas.flowersproject.user.OnItemClickListener;
import com.yumaas.flowersproject.user.TwoImages;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AdminAdsAdapter extends RecyclerView.Adapter<AdminAdsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Ads> ads;

    public AdminAdsAdapter(OnItemClickListener onItemClickListener, ArrayList<Ads> ads) {
        this.onItemClickListener = onItemClickListener;
        this.ads = ads;
    }


    @Override
    public int getItemCount() {
        return ads==null?0:ads.size();
    }



    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Picasso.with(holder.imageView.getContext()).load(ads.get(position).image).into(holder.imageView);
        holder.text.setText(ads.get(position).text);
        holder.delete.setVisibility(View.VISIBLE);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد المسح")
                        .setContentText("بالتاكيد مسح هذا العنصر ؟")
                        .setConfirmText("نعم امسح").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        DataBaseHelper.removeAd(ads.get(position));
                        ads.remove(position);
                        notifyDataSetChanged();
                    }
                })
                        .show();
            }
        });
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView text,delete;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            text = view.findViewById(R.id.text);
            delete= view.findViewById(R.id.delete);

        }
    }
}