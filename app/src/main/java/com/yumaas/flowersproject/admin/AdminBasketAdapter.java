package com.yumaas.flowersproject.admin;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.models.Order;
import com.yumaas.flowersproject.user.OnItemClickListener;

import java.util.ArrayList;


public class AdminBasketAdapter extends RecyclerView.Adapter<AdminBasketAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Order> images;



    public AdminBasketAdapter(OnItemClickListener onItemClickListener, ArrayList<Order> images) {
        this.onItemClickListener = onItemClickListener;


        this.images = images;
    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return images.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_basket, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Picasso.with(holder.itemView.getContext()).load(images.get(position).flower.image).into(holder.imageView);
        holder.name.setText(images.get(position).flower.name);
        holder.details.setText(images.get(position).flower.details);
        holder.price.setText(" سعر البوكيه : " + images.get(position).flower.price + " ريال سعودي ");
        holder.quantity.setText(" الكميه : " + images.get(position).quantity + " ورده ");

        holder.user.setText(" اسم المستخدم : " + images.get(position).user.name  );
        holder.phone.setText(" الهاتف : " + images.get(position).user.phone );

        try {
            int total = Integer.parseInt(images.get(position).flower.price) * images.get(position).quantity;
            holder.total.setText(" الاجمالي : " + total + " ريال سعودي ");
        } catch (Exception e){
            e.getStackTrace();
        }


    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name,details,price,quantity,total,user,phone;



        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            name  = view.findViewById(R.id.name);
            details = view.findViewById(R.id.details);
            price = view.findViewById(R.id.price);
            quantity = view.findViewById(R.id.quantity);
            total = view.findViewById(R.id.total);
            user = view.findViewById(R.id.user);
            phone = view.findViewById(R.id.phone);
        }
    }
}