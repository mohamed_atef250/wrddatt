package com.yumaas.flowersproject.admin;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.user.FlowerDetailsActivity;
 import com.yumaas.flowersproject.user.OnItemClickListener;
import com.yumaas.flowersproject.user.TwoImages;

import java.util.ArrayList;

public class AdminOrdersFragment extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_flowers, container, false);

        RecyclerView recyclerView =  rootView.findViewById(R.id.recycler_view);
        final ArrayList<TwoImages> images = new ArrayList<>();


        for(int i=0; i<10; i++) {
            images.add(new TwoImages(R.drawable.flower1, R.drawable.flowerback1));
            images.add(new TwoImages(R.drawable.flower2, R.drawable.flowerback2));
            images.add(new TwoImages(R.drawable.flower3, R.drawable.flowerback3));
            images.add(new TwoImages(R.drawable.flower4, R.drawable.flowerback4));
            images.add(new TwoImages(R.drawable.flower5, R.drawable.flowerback5));
        }

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

//        FlowersAdapteer categoriesAdapter1 = new FlowersAdapteer(new OnItemClickListener() {
//            @Override
//            public void onItemClickListener(int position) {
//                startActivity(new Intent(getActivity(), FlowerDetailsActivity.class));
//            }
//        }, images);
//        recyclerView.setAdapter(categoriesAdapter1);

        return rootView;
    }

}