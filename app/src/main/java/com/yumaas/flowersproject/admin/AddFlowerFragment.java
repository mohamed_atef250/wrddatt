package com.yumaas.flowersproject.admin;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.flowersproject.ImageResponse;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.base.PopUpItem;
import com.yumaas.flowersproject.base.PopUpMenus;
import com.yumaas.flowersproject.base.Validate;
import com.yumaas.flowersproject.base.filesutils.FileOperations;
import com.yumaas.flowersproject.base.filesutils.VolleyFileObject;
import com.yumaas.flowersproject.databinding.FragmentAddFlowerBinding;
import com.yumaas.flowersproject.models.Flower;

import com.yumaas.flowersproject.user.FragmentHelper;
import com.yumaas.flowersproject.user.OnItemClickListener;
import com.yumaas.flowersproject.user.SweetDialogs;
import com.yumaas.flowersproject.user.TwoImages;
import com.yumaas.flowersproject.volleyutils.ConnectionHelper;
import com.yumaas.flowersproject.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddFlowerFragment extends Fragment {
    private View rootView;
    int catId=-1;
    FragmentAddFlowerBinding fragmentAddFlowerBinding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentAddFlowerBinding   = DataBindingUtil
                .inflate(inflater, R.layout.fragment_add_flower, container, false);

        rootView = fragmentAddFlowerBinding.getRoot();

        fragmentAddFlowerBinding. image.setFocusable(false);
        fragmentAddFlowerBinding.  image.setClickable(true);


        fragmentAddFlowerBinding.category.setFocusable(false);

        fragmentAddFlowerBinding.category.setClickable(true);


        fragmentAddFlowerBinding.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
            }
        });

        fragmentAddFlowerBinding.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    DataBaseHelper.addFlowers(new Flower(fragmentAddFlowerBinding.name.getText().toString()
                    ,selectedImage
                    ,fragmentAddFlowerBinding.details.getText().toString()
                    ,fragmentAddFlowerBinding.price.getText().toString(),catId));

                    SweetDialogs.successMessage(getActivity(), "تم اضافه الورد بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            FragmentHelper.popAllFragments(requireActivity());
                            FragmentHelper.addFragment(requireActivity(),new AdminFlowersFragment(),"AdminFlowersFragment");
                        }
                    });

                }
            }
        });

        fragmentAddFlowerBinding.category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> names = new ArrayList<>();
                names.add("اكاليل");
                names.add("باقات أفراح");
                names.add("باقات أرجوانية");
                names.add("ورد صناعي");
                names.add("أزهار عيد الميلاد");
                names.add("ورد طبيعي");
                ArrayList<PopUpItem>popUpMenus=new ArrayList<>();
                for(int i=0; i<names.size(); i++){
                    popUpMenus.add(new PopUpItem(i,names.get(i)));
                }
            PopUpMenus.showPopUp(requireActivity(),fragmentAddFlowerBinding.category,popUpMenus).setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    catId=menuItem.getItemId();
                    fragmentAddFlowerBinding.category.setText(menuItem.getTitle());
                    return false;
                }
            });
            }
        });

        return rootView;
    }


    private boolean validate(){

        if(catId==-1||Validate.isEmpty(fragmentAddFlowerBinding.image.getText().toString())
                ||Validate.isEmpty(fragmentAddFlowerBinding.name.getText().toString())
                ||Validate.isEmpty(fragmentAddFlowerBinding.details.getText().toString())
                ||Validate.isEmpty(fragmentAddFlowerBinding.price.getText().toString())){
            SweetDialogs.errorMessage(getContext(),"من فضلك قم بملا جميع البيانات");
            return false;
        }

        return true;
    }

    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                FileOperations.getVolleyFileObject(getActivity(), data, "image",
                        43);


        fragmentAddFlowerBinding.image.setText("تم اضافه الصوره بنجاح");

        volleyFileObjects.add(volleyFileObject);



        addServiceApi();


    }


    String selectedImage="";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta",""+selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }






}