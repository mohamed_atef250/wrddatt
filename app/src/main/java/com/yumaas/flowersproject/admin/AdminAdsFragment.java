package com.yumaas.flowersproject.admin;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.databinding.FragmentAddAdsBinding;
import com.yumaas.flowersproject.databinding.FragmentListBinding;
import com.yumaas.flowersproject.user.FlowerDetailsActivity;
import com.yumaas.flowersproject.user.FragmentHelper;
import com.yumaas.flowersproject.user.OnItemClickListener;


public class AdminAdsFragment extends Fragment {
    private View rootView;
    FragmentListBinding fragmentListBinding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentListBinding  = DataBindingUtil
                .inflate(inflater, R.layout.fragment_list, container, false);

        fragmentListBinding.btnAdd.setVisibility(View.VISIBLE);
        fragmentListBinding.btnAdd.setText("اضافه اعلان");
        fragmentListBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(requireActivity(),new AdminAddAdsFragment(),"AdminAddAdsFragment");

            }
        });



        RecyclerView recyclerView =  fragmentListBinding.recyclerView;

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        AdminAdsAdapter categoriesAdapter1 = new AdminAdsAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                startActivity(new Intent(getActivity(), FlowerDetailsActivity.class));
            }
        }, DataBaseHelper.getDataLists().ads);
        recyclerView.setAdapter(categoriesAdapter1);
        rootView=fragmentListBinding.getRoot();
        return rootView;
    }

}