package com.yumaas.flowersproject.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yumaas.flowersproject.admin.AdminFlowersFragment;
import com.yumaas.flowersproject.admin.AdminMainActivity;
import com.yumaas.flowersproject.base.Validate;
import com.yumaas.flowersproject.user.AboutUsFragment;
import com.yumaas.flowersproject.user.BasketFragment;
import com.yumaas.flowersproject.user.FavouritesFragment;
import com.yumaas.flowersproject.user.FragmentHelper;
import com.yumaas.flowersproject.user.HomeFragment;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.user.SearchFragment;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import nl.joery.animatedbottombar.AnimatedBottomBar;

public class MainActivity extends AppCompatActivity {
    EditText text;
    Button search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.tv_navigation_title);
        search = findViewById(R.id.iv_main_logo);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                if(Validate.isEmpty(text.getText().toString())){
                    return;
                }
                bundle.putString("text",text.getText().toString());
                SearchFragment searchFragment= new SearchFragment();
                searchFragment.setArguments(bundle);
                FragmentHelper.addFragment(MainActivity.this,searchFragment,"SearchFragment");
                text.setText("");

                try {
                    View view2 = MainActivity.this.getCurrentFocus();
                    if (view2 != null) {
                        InputMethodManager imm = (InputMethodManager) MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }catch (Exception e){
                    e.getStackTrace();
                }

            }
        });

        AnimatedBottomBar animatedBottomBar = findViewById(R.id.bottom_bar);
        animatedBottomBar.setOnTabSelectListener(new AnimatedBottomBar.OnTabSelectListener() {
            @Override
            public void onTabSelected(int i, @Nullable AnimatedBottomBar.Tab tab, int i1, @NotNull AnimatedBottomBar.Tab tab1) {
                FragmentHelper.popAllFragments(MainActivity.this);
                if(i1==0){
                    FragmentHelper.addFragment(MainActivity.this,new HomeFragment(),"AboutUsFragment");

                }else if(i1==1){
                    FragmentHelper.addFragment(MainActivity.this,new FavouritesFragment(),"AboutUsFragment");

                }else if(i1==2){
                    FragmentHelper.addFragment(MainActivity.this,new BasketFragment(),"BasketFragment");

                }else if(i1==3){
                    FragmentHelper.addFragment(MainActivity.this,new AboutUsFragment(),"AboutUsFragment");
                }else{
                    finish();
                }
            }

            @Override
            public void onTabReselected(int i, @NotNull AnimatedBottomBar.Tab tab) {

            }
        });

        FragmentHelper.addFragment(this,new HomeFragment(),"HomeFragment");
    }

    boolean doubleBackToExitPressedOnce = true;

    @Override
    public void onBackPressed() {


        if (!doubleBackToExitPressedOnce) {
            finish();
        } else {

            this.doubleBackToExitPressedOnce = false;
            Toast.makeText(this, "اشغط مرتين للخروج", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = true, 2000);
          }

    }
}