package com.yumaas.flowersproject.main;


import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.admin.AdminMainActivity;
import com.yumaas.flowersproject.user.SweetDialogs;
import com.yumaas.flowersproject.models.User;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.databinding.FragmentLoginBinding;


public class LoginActivity extends AppCompatActivity {

    FragmentLoginBinding fragmentLoginBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE}, 342);
        }

        fragmentLoginBinding = DataBindingUtil.setContentView(this, R.layout.fragment_login);

        fragmentLoginBinding.btnLoginSignIn.setOnClickListener(view -> {
            if((fragmentLoginBinding.email.getText().toString().equals("admin")||fragmentLoginBinding.email.getText().toString().equals("seller"))&&fragmentLoginBinding.password.getText().toString().equals("123456")){
                Intent intent = new Intent(LoginActivity.this, AdminMainActivity.class);
                startActivity(intent);
            }else{
                User user = DataBaseHelper.loginUser(fragmentLoginBinding.email.getText().toString()
                        , fragmentLoginBinding.password.getText().toString());
                if (user != null) {
                    DataBaseHelper.saveStudent(user);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    SweetDialogs.errorMessage(LoginActivity.this, "ناسف البريد الالكتروني او كلمه السر خطا");
                }
            }
        });


        fragmentLoginBinding.tvLoginRegister.setOnClickListener(view -> startActivity(new Intent(LoginActivity.this, RegisterActivity.class)));


    }


}
