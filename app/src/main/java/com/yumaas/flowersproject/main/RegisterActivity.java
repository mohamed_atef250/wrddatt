package com.yumaas.flowersproject.main;

import android.content.Intent;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.yumaas.flowersproject.R;
import com.yumaas.flowersproject.user.SweetDialogs;
import com.yumaas.flowersproject.models.User;
import com.yumaas.flowersproject.base.DataBaseHelper;
import com.yumaas.flowersproject.base.Validate;
import com.yumaas.flowersproject.databinding.FragmentRegisterBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class RegisterActivity extends AppCompatActivity {

    FragmentRegisterBinding fragmentRegisterBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentRegisterBinding = DataBindingUtil.setContentView(this, R.layout.fragment_register);

        fragmentRegisterBinding.register.setOnClickListener(view -> {
            if(validate()){
                User user = new User(fragmentRegisterBinding.etRegisterName.getText().toString(),
                        fragmentRegisterBinding.etRegisterEmail.getText().toString(),
                        fragmentRegisterBinding.etRegisterPhone.getText().toString(),
                        fragmentRegisterBinding.etRegisterPasswords.getText().toString());
                DataBaseHelper.addUser(user);
                DataBaseHelper.saveStudent(user);
                SweetDialogs.successMessage(RegisterActivity.this, "تم التسجيل بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }




    private boolean validate(){

        if(Validate.isEmpty(fragmentRegisterBinding.etRegisterName.getText().toString())){
            fragmentRegisterBinding.etRegisterName.setError("ادخل الاسم");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.etRegisterEmail.getText().toString())){
            fragmentRegisterBinding.etRegisterEmail.setError("ادخل البريد الالكتروني");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.etRegisterPhone.getText().toString())){
            fragmentRegisterBinding.etRegisterPhone.setError("ادخل رقم الهاتف");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.etRegisterPasswords.getText().toString())){
            fragmentRegisterBinding.etRegisterPasswords.setError("ادخل كلمه المرور");
            return false;
        }else if(!Validate.isMail(fragmentRegisterBinding.etRegisterEmail.getText().toString())){
            fragmentRegisterBinding.etRegisterEmail.setError("البريد الالكتروني خاطئ");
            return false;
        }

        return true;
    }


}
