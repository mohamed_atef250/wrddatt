package com.yumaas.flowersproject.models;

import com.yumaas.flowersproject.base.DataBaseHelper;

import java.io.Serializable;

public class Flower implements Serializable {

    public int id,catId=0;
    public String image,name,details,price;

    public Flower(String name,String image,String details,String price,int catId){
        this.id= DataBaseHelper.generateId();
        this.image=image;
        this.catId=catId;
        this.details=details;
        this.price=price;
        this.name=name;
    }
}
