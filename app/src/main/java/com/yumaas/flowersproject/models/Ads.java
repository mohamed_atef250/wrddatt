package com.yumaas.flowersproject.models;

import com.yumaas.flowersproject.base.DataBaseHelper;

public class Ads {
    public int id;
    public String image,link,text;

    public Ads(String image,String link,String text){
        this.id= DataBaseHelper.generateId();
        this.image=image;
        this.link=link;
        this.text=text;
    }

}
