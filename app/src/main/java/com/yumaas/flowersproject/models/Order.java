package com.yumaas.flowersproject.models;

import com.yumaas.flowersproject.base.DataBaseHelper;

import java.io.Serializable;

public class Order implements Serializable {

    public int id,quantity=0;
    public User user;
    public Flower flower;
    public Order(User user, Flower flower, int quantity){
        this.id= DataBaseHelper.generateId();
        this.user=user;
        this.flower=flower;
        this.quantity=quantity;
    }
}
