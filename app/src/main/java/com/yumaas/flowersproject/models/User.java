package com.yumaas.flowersproject.models;

import android.util.Log;

import com.yumaas.flowersproject.base.DataBaseHelper;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class User implements Serializable {
    public  int id,type;
   public String name,email,phone,password,favourites="",userName,idNumber;
    public double lat, lng;
    public User(String name,String email,String phone,String password){
        this.id= DataBaseHelper.generateId();
        this.name=name;
        this.email=email;
        this.phone=phone;
        this.password=password;
    }

    public void addFavourites(int id){
      User user =   DataBaseHelper.getSavedUser();

      if(!user.favourites.equals(""))
      user.favourites+=","+id;
      else user.favourites+=id;

      DataBaseHelper.saveStudent(user);
    }

    public void deleteFavourites(int id){
        User user =   DataBaseHelper.getSavedUser();
        String favs="";

        List<String>list = Arrays.asList(user.favourites.split(","));
        for(int i=0; i<list.size(); i++){
            Log.d("TAGOZ2",list.get(i));
           if(!list.get(i).equals(id+"")){

               if(!favs.equals("")) {
                   favs += "," + list.get(i);
               }
               else{
                   favs+=list.get(i);
               }

           }
        }

        user.favourites=favs;

        DataBaseHelper.saveStudent(user);
    }

    public boolean checkFavourites(int id){
        User user =   DataBaseHelper.getSavedUser();

        if(user.favourites!=null) {
            Log.d("TAGOZ",user.favourites);
            List<String>list = Arrays.asList(user.favourites.split(","));
            return list.contains(""+id);
        }
        else {
            return false;
        }


    }


}
